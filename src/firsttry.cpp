#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <set>

#include "rand.h"
#include "mymath.h"
#include "utils.h"

#include "Snap.h"
#include "snaphelper.h"
#include "manipulategraph.h"

using namespace std;
using mylib::operator<<;

int main(int argc, char* argv[])
{
    int size(10000);
    int degree(8);
    int threshold = stoi(argv[1]);

//    int upper_bound(100);
//    int repeat(100);

    PUNGraph graph = TSnap::GenRndGnm<PUNGraph>(size, size * degree / 2, false);

    PUNGraph new_graph = copyGraph(graph);

    attackAbove(graph, threshold);

//    for(TUNGraph::TNodeI nodeiter = graph->BegNI(); nodeiter != graph->EndNI(); nodeiter++)
//    {
//        std::cout << nodeiter.GetId() << std::endl;
//    }

    // get all components
    TCnComV comps;
    TSnap::GetSccs(graph, comps);

    // get largest component
    TCnComV::TIter max_iter = comps.BegI();
    for(TCnComV::TIter comp_iter = comps.BegI(); comp_iter != comps.EndI() ; comp_iter++)
    {
        if(max_iter->Len() < comp_iter->Len())
        {
            max_iter = comp_iter;
        }
    }

    set<int> big_neighbors;
    TIntV node_vec = max_iter->NIdV;
    for(TIntV::TIter viter = node_vec.BegI(); viter != node_vec.EndI(); viter++)
    {
//        cout << *viter << " " << new_graph->GetNI(*viter).GetDeg() << endl;
        TUNGraph::TNodeI the_node = new_graph->GetNI(*viter);
        for(int i=0; i<the_node.GetDeg(); ++i)
        {
            TUNGraph::TNodeI neighbor = new_graph->GetNI(the_node.GetOutNId(i));
            if(neighbor.GetDeg() >= threshold)
            {
                big_neighbors.insert(neighbor.GetId());
            }
        }
    }

    cout << "Threshold " << threshold << endl;
    cout << "Node number after attack " <<  graph->GetNodes() << endl;
    cout << "Largest comp " << node_vec.Len() << endl;
    cout << "Big nodes attach to the larges components " << big_neighbors.size() << endl;
    cout << "sum " << graph->GetNodes() + big_neighbors.size() << endl;

    


//    std::ofstream out;
//
//    out.open("./data/comdist12.txt");
//
//    componentDist(out, 12, repeat, degree, size);
//
//    out.close();

//    cout << graph->GetNodes() << endl;
//    cout << TSnap::GetMxSccSz(graph) << endl;


//    int size(1000);
//    int degree(8);
//
//    PUNGraph graph = TSnap::GenRndGnm<PUNGraph>(size, size * degree / 2, false);
////
//    cout << TSnap::GetMxSccSz(graph) << endl;
//    attackOnRatio(graph, 0.8);
//    cout << TSnap::GetMxSccSz(graph) << endl; 
//    cout << graph->GetNodes() << endl;

//    randomDeleteNodes(graph, 0.1);
//
//    int currentsize = graph->GetNodes();
//
//    cout << currentsize * TSnap::GetMxSccSz(graph) / static_cast<double>(size) << endl;


//    std::string file_name = "./data/before.out";
//    std::ofstream out;
//
//    out.open(file_name);
//
//    degreeDist(graph, out);
//
//    out.close();

    // find the max degree

    // remove the vertice whose degree is larger than i



//    
//    file_name = "./data/after.out";
//
//    out.open(file_name);
//
//    degreeDist(graph, out);
//
//    out.close();

    return 0;
}
