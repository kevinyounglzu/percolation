#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <set>

#include "rand.h"
#include "mymath.h"
#include "utils.h"

#include "snaphelper.h"
#include "manipulategraph.h"
#include "Snap.h"

using namespace std;
using mylib::operator<<;

int main(int argc, char* argv[])
{
    int size(1000);
    double dsize = static_cast<double>(size);
    int degree(8);
    int upper_bound(100);
    int repeat(10000);
    double drepeat = static_cast<double>(repeat);

    std::ofstream out;

    out.open("./data/attackresult.txt");

    for(int i=0; i<upper_bound; ++i)
    {
        cout << i << endl;
        double rest_base(0);
        double largest_comp(0);
        for(int j=0; j<repeat; ++j)
        {
            PUNGraph graph = TSnap::GenRndGnm<PUNGraph>(size, size * degree / 2, false);
            attackAbove(graph, i);
            rest_base += graph->GetNodes() / dsize;
            largest_comp += graph->GetNodes() * TSnap::GetMxSccSz(graph) / dsize;
        }
        out << i << " " << rest_base / drepeat << " " << largest_comp / drepeat << endl;
        
    }

    out.close();






//
//    componentDist(out, 12, repeat, degree, size);
//
//    out.close();

//    cout << graph->GetNodes() << endl;
//    cout << TSnap::GetMxSccSz(graph) << endl;


//    int size(1000);
//    int degree(8);
//
//    PUNGraph graph = TSnap::GenRndGnm<PUNGraph>(size, size * degree / 2, false);
////
//    cout << TSnap::GetMxSccSz(graph) << endl;
//    attackOnRatio(graph, 0.8);
//    cout << TSnap::GetMxSccSz(graph) << endl; 
//    cout << graph->GetNodes() << endl;

//    randomDeleteNodes(graph, 0.1);
//
//    int currentsize = graph->GetNodes();
//
//    cout << currentsize * TSnap::GetMxSccSz(graph) / static_cast<double>(size) << endl;


//    std::string file_name = "./data/before.out";
//    std::ofstream out;
//
//    out.open(file_name);
//
//    degreeDist(graph, out);
//
//    out.close();

    // find the max degree

    // remove the vertice whose degree is larger than i



//    
//    file_name = "./data/after.out";
//
//    out.open(file_name);
//
//    degreeDist(graph, out);
//
//    out.close();

    return 0;
}
