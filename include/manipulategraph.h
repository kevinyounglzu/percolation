#ifndef MANIPGRAPH_H
#define MANIPGRAPH_H

#include <iostream>
#include "rand.h"
#include "Snap.h"
#include "mymath.h"
#include <vector>
#include <map>
//#include <algorithm>
#include <utility>

void randomDeleteNodes(PUNGraph graph, double p)
{
    mylib::RandDouble randd;

    std::vector<int> node_to_delete;

    for(TUNGraph::TNodeI nodeiter = graph->BegNI(); nodeiter != graph->EndNI(); nodeiter++)
    {
        if(randd.gen() < p)
            node_to_delete.push_back(nodeiter.GetId());
    }

    for(int id : node_to_delete)
        graph->DelNode(id);
}

bool cmp(const std::pair<int, int> & left, const std::pair<int, int> & right)
{
    return left.first > right.first;
}

//void attackOnRatio(PUNGraph graph, double p)
//{
//    std::vector<std::pair<int, int>> storage;
//    
//    for(TUNGraph::TNodeI nodeiter = graph->BegNI(); nodeiter != graph->EndNI(); nodeiter++)
//    {
//        storage.push_back(std::make_pair(nodeiter.GetDeg(), nodeiter.GetId()));
//    }
//
//    std::sort(storage.begin(), storage.end(), cmp);
//
//    int number_to_delete = static_cast<int>(graph->GetNodes() * p);
//
//    for(int i=0; i<number_to_delete; ++i)
//    {
//        graph->DelNode(storage[i].second);
//    }
////    for(auto p: storage)
////    {
////        std::cout << p.first << " " << p.second << std::endl;
////    }
//
//}

void attackAbove(PUNGraph graph, int upper_bound)
{
    std::vector<int> node_to_delete;

    for(TUNGraph::TNodeI nodeiter = graph->BegNI(); nodeiter != graph->EndNI(); nodeiter++)
    {
        if(nodeiter.GetDeg() >= upper_bound)
            node_to_delete.push_back(nodeiter.GetId());
    }

    for(int id : node_to_delete)
        graph->DelNode(id);
}

PUNGraph copyGraph(PUNGraph graph)
{
    PUNGraph new_graph = TUNGraph::New();
    for(TUNGraph::TNodeI nodeiter = graph->BegNI(); nodeiter != graph->EndNI(); nodeiter++)
    {
        new_graph->AddNode(nodeiter.GetId());
    }

    for(TUNGraph::TEdgeI edgeiter = graph->BegEI(); edgeiter != graph->EndEI(); edgeiter++)
    {
        new_graph->AddEdge(edgeiter.GetSrcNId(), edgeiter.GetDstNId());
    }

    return new_graph;
}

void componentDist(std::ostream& out, int upper_bound, int repeat, int degree, int size)
{

    std::map<int, int> dist;

    for(int i=0; i<repeat; ++i)
    {
        PUNGraph graph = TSnap::GenRndGnm<PUNGraph>(size, size * degree / 2, false);
        attackAbove(graph, upper_bound);

        TIntPrV result;
        TSnap::GetSccSzCnt(graph, result);

        for(auto iter = result.BegI(); iter != result.EndI(); iter++)
        {
            if(iter->GetVal2())
            {
                auto map_iter = dist.find(iter->GetVal1());
                if(map_iter != dist.end())
                {
                    map_iter->second += iter->GetVal2();
                }
                else
                    dist.insert(std::make_pair(iter->GetVal1(), iter->GetVal2()));
            }
        }
    }

    for(auto map_iter : dist)
    {
        out << map_iter.first << " " << map_iter.second << std::endl;
    }
}

double deleteNodes(PUNGraph graph, double p, void (*delete_method)(PUNGraph _graph, double _p))
{
    double origin_size = static_cast<double>(graph->GetNodes());
    delete_method(graph, p);
    return graph->GetNodes() * TSnap::GetMxSccSz(graph) / origin_size;
}

//void ERdelete(int size, int degree, int repeat)
//{
//
//    for(double p : mylib::linspace(0, 1., 100))
//    {
//        double base(0);
//        for(int i=0; i<repeat; ++i)
//        {
//            PUNGraph graph = TSnap::GenRndGnm<PUNGraph>(size, size * degree / 2, false);
//            base += deleteNodes(graph, p, attackOnRatio);
//        }
//        std::cout << p << " " << base / static_cast<double>(repeat) << std::endl;
//    }
//}

void ERattackAbove(int size, int degree, int repeat, int upper_bound)
{

    for(int u=0; u<upper_bound; ++u)
    {
        double base(0);
        for(int i=0; i<repeat; ++i)
        {
            PUNGraph graph = TSnap::GenRndGnm<PUNGraph>(size, size * degree / 2, false);
            attackAbove(graph, u);
            base += graph->GetNodes() * TSnap::GetMxSccSz(graph) / static_cast<double>(size);
        }
        std::cout << u << " " << base / static_cast<double>(repeat) << std::endl;
    }

}

void compLargerThanThreshold(int size, int degree, int repeat, int upper_bound, int threshold)
{
    for(int u=0; u<upper_bound; ++u)
    {
        double base(0);
        for(int i=0; i<repeat; ++i)
        {
            PUNGraph graph = TSnap::GenRndGnm<PUNGraph>(size, size * degree / 2, false);
            attackAbove(graph, u);

            TIntPrV result;
            TSnap::GetSccSzCnt(graph, result);

            double one_base(0);
            for(auto iter = result.BegI(); iter != result.EndI(); iter++)
            {
                if(iter->GetVal2())
                {
                    if(iter->GetVal1() > threshold)
                    {
                        one_base += iter->GetVal1() * iter->GetVal2() / static_cast<double>(size);
                    }
                }
            }

            base += one_base;
        }
        std::cout << u << " " << base / static_cast<double>(repeat) << std::endl;
    }
}


#endif
