#ifndef SNAPHELPER_H
#define SNAPHELPER_H

#include <map>
#include <vector>
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include "mymath.h"

#include "Snap.h"

// some helper functions
void ExecessDegreeDist(const PUNGraph & graph, std::ostream & out)
{
    int sum(0), degree(0);
    std::map<int, std::vector<int>> store;

    for(TUNGraph::TNodeI nodeiter = graph->BegNI(); nodeiter != graph->EndNI(); nodeiter++)
    {
        // iterate through the node's neighbors
        degree = nodeiter.GetDeg();
        auto it = store.find(degree);
        if(it == store.end())
        {
            store[degree] = std::vector<int>();
        }
        sum = 0;
        for(int i=0; i<degree; ++i)
        {
            sum += graph->GetNI(nodeiter.GetNbrNId(i)).GetDeg();
        }
        store[degree].push_back(sum / static_cast<double>(degree));
    }
    for(auto & iter: store)
    {
        out << iter.first << " " << mylib::average(iter.second) << std::endl;
    }
}

void degreeDist(const PUNGraph & graph, std::ostream & out)
{
    TVec<TPair<TInt, TInt>> result;
    TSnap::GetOutDegCnt(graph, result);

    for(auto iter = result.BegI(); iter != result.EndI(); iter++)
    {
        out << iter->GetVal1();
        if(iter->GetVal2())
            out << " " << iter->GetVal2() <<std::endl;
        else
            out << " " << 0 << std::endl;
    }
}

void clusterDist(const PUNGraph & graph, std::ostream & out)
{
    TIntPrV cluster_dist;
    TSnap::GetSccSzCnt(graph, cluster_dist);

    for(auto iter = cluster_dist.BegI(); iter != cluster_dist.EndI(); iter++)
    {
        out << iter->GetVal1() << " " << iter->GetVal2() << std::endl;
    }
}


void profileNetworks(const PUNGraph & graph, std::string graph_name)
{
    // degree distibution
    std::string filename_base = "./data/";
    std::string file_suffix = ".out";
    std::ostringstream file_name;
    file_name << filename_base << graph_name << "_dist" << file_suffix;
    std::ofstream out;
    out.open(file_name.str());

    degreeDist(graph, out);

    out.close();

    // correlation between degrees
    file_name.str(std::string());
    file_name << filename_base << graph_name << "_exess" << file_suffix;
    out.open(file_name.str());

    ExecessDegreeDist(graph, out);

    out.close();
}

//double averageDegree(const PUNGraph & graph)
//{
//    TVec<TPair<TInt, TInt>> result;
//    TSnap::GetOutDegCnt(graph, result);
//
//    for(auto iter = result.BegI(); iter != result.EndI(); iter++)
//    {
//        out << iter->GetVal1();
//        if(iter->GetVal2())
//            out << " " << iter->GetVal2() <<std::endl;
//        else
//            out << " " << 0 << std::endl;
//    }
//
//}

double averageShortedPath(const PUNGraph & graph, bool verbose=false)
{
    int n = graph->GetNodes();
    int base(0);

    for(int i=0; i<n; ++i)
    {
        if(verbose)
            std::cout << "Node " << i << "..." << std::endl;
        for(int j=i+1; j<n; ++j)
        {
            base += TSnap::GetShortPath(graph, i, j);
        }
    }
    return base / static_cast<double>(n * (n - 1)/2);
}

int genNetworks()
{
    //    // for parameters
    //    mylib::Parser parser("./config.cfg");
    //    int n = parser.getInt("n");
    //    double p = parser.getDouble("p");

    //    // er random networks
    //    PUNGraph er_graph;
    //    er_graph = TSnap::GenRndGnm<PUNGraph>(n, static_cast<int>(n * (n-1)/2. * p), false);
    //    profileNetworks(er_graph, "er");
    //
    //    // power law networks
    //    PUNGraph power_graph;
    //    power_graph = TSnap::GenRndPowerLaw(n, 2.2);
    //    profileNetworks(power_graph, "powerlaw");
    //
    //    // regular random networks
    //    PUNGraph regular_random;
    //    regular_random = TSnap::GenRndDegK(1000, 4);
    //    profileNetworks(regular_random, "regular_random");
    //
    //    // BA
    //    PUNGraph ba_graph;
    //    ba_graph = TSnap::GenPrefAttach(10000, 4);
    //    profileNetworks(ba_graph, "ba");
    //
    //    // rewrite BA
    //    PUNGraph rewrite_ba = TSnap::GenRewire(ba_graph);
    //    profileNetworks(rewrite_ba, "rewrite_ba");
    return 0;
}

PUNGraph genOneDLattice(int N, bool cycle=false)
{
    PUNGraph graph = TUNGraph::New();
    // add nodes
    for(int i=0; i<N; ++i)
    {
        graph->AddNode(i);
    }
    // add edges
    for(int i=0; i<N; ++i)
    {
        if(i > 0)
            graph->AddEdge(i, i-1);
        if(i<N-1)
            graph->AddEdge(i, i+1);
    }

    if(cycle)
    {
        graph->AddEdge(0, N-1);
    }

    return graph;
}

#endif
